document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();
    var taskInput = document.getElementById('task-input');
    var taskList = document.getElementById('task-list');
    if (taskInput.value) {
        var newTask = document.createElement('li');
        newTask.textContent = taskInput.value;
        newTask.classList.add('border', 'border-gray-400', 'p-2', 'my-2', 'rounded');
        var deleteButton = document.createElement('button');
        deleteButton.innerHTML = '<i class="fas fa-trash-alt"></i>';
        deleteButton.classList.add('float-right', 'text-red-500');
        deleteButton.addEventListener('click', function() {
            taskList.removeChild(newTask);
        });
        newTask.appendChild(deleteButton);
        taskList.appendChild(newTask);
        taskInput.value = '';
    }
});
